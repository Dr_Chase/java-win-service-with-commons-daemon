package app;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
//import org.joda.time.LocalDateTime;

public class Application {
	
	private static Application serviceApp = new Application();
	
	private boolean stopped = false;
	
	private static LocalDateTime nowDateTime;
	
	private static PrintWriter pWriter = null;

	public static void main(String[] args) {
		
		try {
			pWriter = new PrintWriter("logging.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			for (;;) {
				nowDateTime = LocalDateTime.now();
				pWriter.println("Current Time is: " + nowDateTime);
				pWriter.flush();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally	{
			pWriter.close();
		}
		
	}
	
	public void start(String[] args)	{
		stopped = false;
		
		Application.main(args);
	}
	
	public void stop(String[] args)	{
		stopped = true;
		pWriter.close();
		System.exit(0);
	}
	
	public static void windowsService(String args[]) {
	      String cmd = "start";
	      if(args.length > 0) {
	         cmd = args[0];
	      }
		
	      if("start".equals(cmd) ) {
	    	  serviceApp.start(args);
	      }
	      else {
	    	  serviceApp.stop(args);
	      }
	   }
	

}
