pushd ..
set "ROOT_DIR=%cd%"
popd

set BIN_DIR=%CD%
rem set ROOT_DIR=%~dp0\..
rem set PR_JVM=%CD%\..
set "newDir=%~dp0\..\"

prunsrv.exe //IS//ChaseDummyLogService --Install=%CD%\prunsrv.exe --Description="A fake log service for testing." --Jvm=%ROOT_DIR%\dist\jre1.8.0_111\bin\server\jvm.dll --Classpath=%ROOT_DIR%\target\upload-agent-dummy-0.0.1.jar --StartMode=jvm --StartClass=app.Application --StartMethod=windowsService --StartParams=start --StopMode=jvm --StopClass=app.Application --StopMethod=windowsService --StopParams=stop --LogPath=%CD%\..\logs --StdOutput=auto --StdError=auto